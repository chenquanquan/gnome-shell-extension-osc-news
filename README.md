## gnome-shell-extension-osc-news
 *gnome-shell-extension-osc-news* is a simple extension for displaying oschina news in GNOME Shell 
 
 ![Screenshot](data/screenshot.png)


## Installation

Run the following commands:

``` Bash
cd ~/.local/share/gnome-shell/extensions && git clone https://gitee.com/chenchacha/gnome-shell-extension-osc-news.git osc-news@mt6276m.org
```

Restart the shell and then enable the extension.
